package edu.pja.sri.mdziurzynski.s22012.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Apartment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String streetName;
    private String streetNumber;
    private String flatNumber;
    private String zipCode;
    private String city;
    private String country;
    private Integer numberOfResidents;
    private Integer numberOfRooms;
    private Double areaInSquareMeters;
    private Boolean hasBalcony;
    private LocalDate constructionDate;
}
