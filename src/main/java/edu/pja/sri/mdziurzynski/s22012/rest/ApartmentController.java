package edu.pja.sri.mdziurzynski.s22012.rest;

import edu.pja.sri.mdziurzynski.s22012.dto.ApartmentDto;
import edu.pja.sri.mdziurzynski.s22012.model.Apartment;
import edu.pja.sri.mdziurzynski.s22012.repo.ApartmentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/apartments")
public class ApartmentController {
    private ApartmentRepository apartmentRepository;
    private ModelMapper modelMapper;

    public ApartmentController(ApartmentRepository apartmentRepository, ModelMapper modelMapper) {
        this.apartmentRepository = apartmentRepository;
        this.modelMapper = modelMapper;
    }

    public ApartmentDto convertToDto(Apartment apartment) {
        return modelMapper.map(apartment, ApartmentDto.class);
    }

    public Apartment convertToEntity(ApartmentDto apartmentDto) {
        return modelMapper.map(apartmentDto, Apartment.class);
    }

    @GetMapping
    public ResponseEntity<Collection<ApartmentDto>> getApartments() {
        List<Apartment> allApartments = apartmentRepository.findAll();
        List<ApartmentDto> allApartmentsDto = allApartments.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(allApartmentsDto, HttpStatus.OK);
    }

    @GetMapping("/{apartmentId}")
    public ResponseEntity<ApartmentDto> getApartmentById(@PathVariable Long apartmentId) {
        Optional<Apartment> apartment = apartmentRepository.findById(apartmentId);
        if (apartment.isPresent()) {
            ApartmentDto apartmentDto = convertToDto(apartment.get());
            return new ResponseEntity<>(apartmentDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity addApartment(@RequestBody ApartmentDto apartmentDto) {
        Apartment apartmentEntity = convertToEntity(apartmentDto);
        apartmentRepository.save(apartmentEntity);

        HttpHeaders headers = new HttpHeaders();
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(apartmentEntity.getId())
                .toUri();
        headers.add("Apartment", location.toString());

        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @PutMapping("/{apartmentId}")
    public ResponseEntity updateApartment(@PathVariable Long apartmentId, @RequestBody ApartmentDto apartmentDto) {
        Optional<Apartment> currentApartment = apartmentRepository.findById(apartmentId);
        if (currentApartment.isPresent()) {
            apartmentDto.setId(apartmentId);
            Apartment apartmentEntity = convertToEntity(apartmentDto);
            apartmentRepository.save(apartmentEntity);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{apartmentId}")
    public ResponseEntity deleteApartment(@PathVariable Long apartmentId) {
        Optional<Apartment> currentApartment = apartmentRepository.findById(apartmentId);
        if (currentApartment.isPresent()) {
            apartmentRepository.deleteById(apartmentId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/residents")
    public ResponseEntity getTotalResidents() {
        List<Apartment> allApartments = apartmentRepository.findAll();
        Integer totalResidents = allApartments.stream()
                .map(Apartment::getNumberOfResidents)
                .reduce(0, Integer::sum);
        HashMap<String, Integer> result = new HashMap<>();
        result.put("totalResidents", totalResidents);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/area")
    public ResponseEntity getAverageArea() {
        List<Apartment> allApartments = apartmentRepository.findAll();
        Double totalArea = allApartments.stream()
                .map(Apartment::getAreaInSquareMeters)
                .reduce(0.0, Double::sum);
        HashMap<String, Double> result = new HashMap<>();
        result.put("totalArea", totalArea);

        if (allApartments.size() == 0) {
            result.put("averageArea", 0.0);
        } else {
            result.put("averageArea", totalArea / (double) allApartments.size());
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
