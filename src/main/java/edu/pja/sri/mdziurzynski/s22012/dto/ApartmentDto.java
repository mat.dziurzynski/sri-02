package edu.pja.sri.mdziurzynski.s22012.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApartmentDto {
    private Long id;
    private String streetName;
    private String streetNumber;
    private String flatNumber;
    private String zipCode;
    private String city;
    private String country;
    private Integer numberOfResidents;
    private Integer numberOfRooms;
    private Double areaInSquareMeters;
    private Boolean hasBalcony;
    private LocalDate constructionDate;
}
